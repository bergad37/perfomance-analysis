### Setting up the project 

1. clone the project 
2. install the packages npm install
3. install matplotlib by `pip3 install matplotlib`
4. Add log's file path in the `analytic.py` file
5. Run the command with `python3 analytic.py` , make sure it python3 is installed if not , use [download python3](https://www.python.org/)

